local treesitter = require('nvim-treesitter.configs')

treesitter.setup {
	ensure_installed = { 'c', 'lua', 'vim', 'vimdoc', 'query', 'python' },
	sync_install = false,
	auto_install = true,		-- set to false if tree-sitter CLI is not installed
	ignore_install = {},

	highlight = {
		enable = true,
	},
	indent = {
		enable = true,
	},
}
