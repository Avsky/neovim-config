-- Completion --

local cmp = require('cmp')
local lspkind = require('lspkind')

-- gray
vim.api.nvim_set_hl(0, 'CmpItemAbbrDeprecated', { bg='NONE', strikethrough=true, fg='#808080' })
-- blue
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatch', { bg='NONE', fg='#569CD6' })
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatchFuzzy', { link='CmpIntemAbbrMatch' })
-- light blue
vim.api.nvim_set_hl(0, 'CmpItemKindVariable', { bg='NONE', fg='#9CDCFE' })
vim.api.nvim_set_hl(0, 'CmpItemKindInterface', { link='CmpItemKindVariable' })
vim.api.nvim_set_hl(0, 'CmpItemKindText', { link='CmpItemKindVariable' })
-- pink
vim.api.nvim_set_hl(0, 'CmpItemKindFunction', { bg='NONE', fg='#C586C0' })
vim.api.nvim_set_hl(0, 'CmpItemKindMethod', { link='CmpItemKindFunction' })
-- front
vim.api.nvim_set_hl(0, 'CmpItemKindKeyword', { bg='NONE', fg='#D4D4D4' })
vim.api.nvim_set_hl(0, 'CmpItemKindProperty', { link='CmpItemKindKeyword' })
vim.api.nvim_set_hl(0, 'CmpItemKindUnit', { link='CmpItemKindKeyword' })

cmp.setup {
	snippet = {
		expand = function(args)
			require('luasnip').lsp_expand(args.body)
		end,
	},
	window = {
		-- completion = cmp.config.window.bordered(),
		-- documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert({
		['<C-b>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = false }),
		['<TAB>'] = cmp.mapping.select_next_item(),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
	}, {
		{ name = 'buffer' },
	}),
	view = {
		entries = {
			name = 'custom',
			selection_order = 'near_cursor',
		}
	},
	formatting = {
		fields = { 'kind', 'abbr', 'menu' },
		format = lspkind.cmp_format({
			mode = 'symbol_text',
			menu = ({
				buffer = '[Buffer]',
				nvim_lsp = '[LSP]',
				luasnip = '[LuaSnip]',
				nvim_lua = '[Lua]',
				git = '[Git]',
			}),
			maxwidth = 50,
		})
	},
}

cmp.setup.filetype('gitcommit', {
	sources = cmp.config.sources({
		{ name = 'git' },
	}, {
		{ name = 'buffer' },
	})
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
	mapping = cmp.mapping.preset.cmdline(),
	sources = {
		{ name = 'buffer' }
	},
	formatting = {
		fields = { 'abbr', 'menu' }
	},
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = 'path' }
	}, {
		{ name = 'cmdline' }
	}),
	formatting = {
		fields = { 'abbr' },
	},
})

local cmp_cap = require('cmp_nvim_lsp').default_capabilities()

-- Servers --

local lspconfig = require('lspconfig')
local lsputil = lspconfig.util

lspconfig.ccls.setup {
	capabilities = cmp_cap,
	init_options = {
		compilationDatabaseDirectory = "";
		index = {
			threads = 0;
			comments = 1;
		};
		cache = {
			directory = "/tmp/ccls-cache";
		};
		highlight = {
			lsRanges = true;
		};
	},
}

lspconfig.pylsp.setup {
	capabilities = cmp_cap,
	settings = {
		pylsp = {
			enable = true;
			trace = {
				server = "verbose";
			};
			plugins = {
				jedi_completion = {
					enabled = true;
				};
				jedi_hover = {
					enabled = true;
				};
				jedi_references = {
					enabled = true;
				};
				jedi_signature_help = {
					enabled = true;
				};
				jedi_symbols = {
					enabled = true;
					all_scopes = true;
				};
				mccabe = {
					enabled = true;
					threshold = 15;
				};
				preload = {
					enabled = true;
				};
				pycodestyle = {
					enabled = true;
				};
				pydocstyle = {
					enabled = false;
					match = "(?!test_).*\\.py";
					matchDir = "[^\\.].*";
				};
				pyflakes = {
					enabled = true;
				};
				rope_completion = {
					enabled = true;
				};
				yapf = {
					enabled = true;
				}
			}
		}
	},
}

lspconfig.zls.setup {
	capabilities = cmp_cap,
	root_dir = lsputil.root_pattern('zls.json', 'build.zig'),
}

lspconfig.bashls.setup {
	capabilities = cmp_cap,
}

lspconfig.cmake.setup {
	capabilities = cmp_cap,
}

lspconfig.lua_ls.setup {
	capabilities = cmp_cap,
	on_init = function(client)
		-- check and setup for editing Neovim configs
		local path = client.workspace_folders[1].name
		if not vim.loop.fs_stat(path .. '/.luarc.json') and not vim.loop.fs_stat(path .. '/.luarc.jsonc') then
			client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
				Lua = {
					runtime = {
						-- change when nvim moves to Lua5.4
						version = 'LuaJIT'
					},
					-- Neovim runtime
					workspace = {
						checkThirdParty = false,
						library = {
							vim.env.VIMRUNTIME
							-- "${3rd}/luv/library"
							-- "${3rd}/busted/library",
						}
					}
				}
			})

			client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
		end
		return true
	end
}
