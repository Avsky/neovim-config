-- Neo-tree --
vim.keymap.set('n', '<leader>fs', ':Neotree reveal toggle right<CR>', { noremap = true })
vim.keymap.set('n', '<leader>gs', ':Neotree float source=git_status<CR>', { noremap = true })

-- Telescope --
local ts_builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', ts_builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', ts_builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', ts_builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', ts_builtin.help_tags, {})
vim.keymap.set('n', '<leader>fG', ts_builtin.git_commits, {})

-- Neovim LSP --
-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Python Venv Selector --
vim.keymap.set('n', '<leader>vs', '<cmd>VenvSelect<CR>', { noremap = true })
vim.keymap.set('n', '<leader>vc', '<cmd>VenvSelectCached<CR>', { noremap = true })

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
	group = vim.api.nvim_create_augroup('UserLspConfig', {}),
	callback = function(ev)
		-- Enable completion triggered by <c-x><c-o>
		vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

		-- Make hover popup focusable
		vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
			vim.lsp.handlers.hover, { focusable = true }
		)

		-- Buffer local mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local opts = { buffer = ev.buf }
		vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
		vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
		vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
		vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
		vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts)
		vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
		vim.keymap.set('n', '<leader>wl', function()
				print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
			end, opts)
		vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, opts)
		vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
		vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
		vim.keymap.set('n', '<leader>f', function()
				vim.lsp.buf.format { async = true }
			end, opts)

		-- Trouble --
		local trouble = require('trouble')
		vim.keymap.set('n', 'td', function() trouble.toggle('diagnostics', 'filter.buf=0') end)
		vim.keymap.set('n', 'tD', function() trouble.toggle('diagnostics') end)
		vim.keymap.set('n', 'gr', function() trouble.toggle('lsp_references') end)
		vim.keymap.set('n', '<leader>qf', function() trouble.toggle('qflist') end)
		vim.keymap.set('n', '<leader>sl', function() trouble.toggle('symbols', 'focus=false') end)

		-- Telescope --
		vim.keymap.set('n', '<leader>sym', ts_builtin.lsp_dynamic_workspace_symbols, {})
	end,
})
