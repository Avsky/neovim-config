local opts = {}

local plugins = {
	-- Look and feel --
	'iCyMind/NeoSolarized',
	'joshdick/onedark.vim',
	'tomasr/molokai',
	'folke/tokyonight.nvim',
	'nvim-tree/nvim-web-devicons',
	{
		'nvim-lualine/lualine.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' }
	},
	'onsails/lspkind.nvim', -- for completion icons
	{
		'j-hui/fidget.nvim',
		config = function()
			local fidget = require('fidget')
			fidget.setup {
				progress = {
					poll_rate = 0,
					ignore = {},
					display = {
						render_limit = 10,
						skip_history = true,
					},
					lsp = {
						progress_ringbuf_size = 8
					}
				},
				notification = { view = {
					render_message =
						function(msg, cnt)
							return cnt == 1 and msg or string.format("(%dx) %s", cnt, msg)
						end,
				} },
				integration = {}
			}
			-- Override default Neovim notifications
			vim.notify = fidget.notification.notify
		end
	},
	-- Lang Syntax --
	'sheerun/vim-polyglot',
	'ziglang/zig.vim',
	-- Tools --
	{
		'nvim-neo-tree/neo-tree.nvim',
		branch = 'v3.x',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'nvim-tree/nvim-web-devicons',
			'MunifTanjim/nui.nvim'
		},
		config = function()
			require('neo-tree').setup {
				close_if_last_window = false,
				popup_border_style = 'rounded',
				enable_git_status = true,
				enable_diagnostics = true,
				name = {
					trailing_slash = true,
					use_git_status_colors = true,
					highlight = 'NeoTreeFileName'
				}
			}
		end
	},
	'terryma/vim-multiple-cursors',
	{
		'airblade/vim-gitgutter',
		branch = 'main'
	},
	'tpope/vim-fugitive',
	{
		'nvim-telescope/telescope.nvim', branch = '0.1.x',
		dependencies = { 'nvim-lua/plenary.nvim' }
	},
	{
		'linux-cultist/venv-selector.nvim',
		lazy = false,
		branch = "regexp",
		config = function()
			require('venv-selector').setup {
				auto_refresh = false
			}
		end,
	},
	-- LSP and code intelligence --
	'neovim/nvim-lspconfig',
	{
		'nvim-treesitter/nvim-treesitter',
		build = function()
			local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
			ts_update()
		end,
		config = function()
			-- WORKAROUND
			vim.api.nvim_create_autocmd({'BufEnter','BufAdd','BufNew','BufNewFile','BufWinEnter'}, {
				group = vim.api.nvim_create_augroup('TS_FOLD_WORKAROUND', {}),
				callback = function()
				vim.opt.foldmethod	= 'expr'
				vim.opt.foldexpr	= 'nvim_treesitter#foldexpr()'
				end
			})
		end,
	},
	'm-pilia/vim-ccls',
	{
		'folke/trouble.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' }
	},
	-- completion --
	'hrsh7th/cmp-nvim-lsp',
	'hrsh7th/cmp-buffer',
	'hrsh7th/cmp-path',
	'hrsh7th/cmp-cmdline',
	'petertriho/cmp-git',
	'hrsh7th/nvim-cmp',
	-- snippets --
	{
		'L3MON4D3/LuaSnip',
		dependencies = {
			-- extra snippets
			'rafamadriz/friendly-snippets'
		},
		config = function()
			require('luasnip.loaders.from_vscode').lazy_load()
		end
	},
	'saadparwaiz1/cmp_luasnip'
}

return require('lazy').setup(plugins, opts)
