vim.cmd([[
syntax enable
colorscheme tokyonight
filetype plugin indent on
]])

vim.opt.encoding = 'utf-8'

vim.opt.cursorline = true
vim.opt.cursorcolumn = true
vim.opt.list = true
vim.opt.foldenable = false
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.tabstop = 8
vim.opt.termguicolors = true
vim.opt.scrolloff = 5
vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.pumheight = 15

vim.opt.mouse = 'a'
vim.opt.updatetime = 300

require('statusline')
require('treesitter')
require('icons')
require('lsp')
