local function current_treesitter_context()
	if not packer_plugins["nvim-treesitter"] or packer_plugins["nvim-treesitter"].loaded == false then
		return "TS not available"
	end
	local f = require('nvim-treesitter').statusline({
		indicator_size = 300,
		type_patterns = {"class", "function", "method", "interface"}
	})
	local context = string.format("%s", f)

	if context == "nil" then
		return "TS not available"
	end
	return context
end

local function lsp_clients()
	local clients = vim.lsp.buf_get_clients()
	if #clients == 0 then
		return ''
	end

	local lsps = {}
	for _,client in pairs(clients) do
		table.insert(lsps, client.name)
	end

	return '󰱓 ' .. table.concat(lsps, ' | ')
end

require('lualine').setup {
	options = {
		icons_enabled = true,
		theme = 'tokyonight',
		component_separators = { left = '', right = ''},
		section_separators = { left = '', right = ''},
		disabled_filetypes = {
		statusline = {},
		winbar = {},
	},
	ignore_focus = {},
	always_divide_middle = true,
	globalstatus = false,
	refresh = {
		statusline = 1000,
		tabline = 1000,
		winbar = 1000,
	}

	},
	sections = {
		lualine_a = {'mode'},
		lualine_b = {
			'branch',
			'diff',
			{
				'diagnostics',
				sources = { 'nvim_lsp' },
				sections = { 'error', 'warn', 'info', 'hint' },
				--symbols = {
				--	error = 'E',
				--	warn = 'W',
				--	info = 'I',
				--	hint = 'H'
				--},
				colored = true,
				update_in_insert = true,
				always_visible = false
			}
		},
		lualine_c = {
			'filename',
			-- 'nvim_treesitter#statusline'
			current_treesitter_context,
		},
		lualine_x = {
			'encoding',
			{
				'fileformat',
				symbols = {
					unix = 'unix',
					dos = 'dos',
					mac = 'mac'
				}
			},
			'filetype'
		},
		lualine_y = {
			'progress',
			'searchcount'
		},
		lualine_z = {'location'}
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {}
	},
	tabline = {
		lualine_a = {
			{
				'buffers',
				show_filename_only = true,
				mode = 2,
				show_modified_status = true,
				use_mode_colors = true,
				symbols = {
					modified = ' *',      -- Text to show when the buffer is modified
					alternate_file = '', -- Text to show to identify the alternate file
					directory = ' D',     -- Text to show when the buffer is a directory
				}
			}
		},
		lualine_z = { lsp_clients }
	},
	winbar = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {}
	},
	inactive_winbar = {},
	extensions = { 'fugitive', 'neo-tree', 'quickfix', 'trouble' }
}
