if exists('g:GuiLoaded')
	Guifont! CaskaydiaCove Nerd Font:h10
	set background=dark

	GuiTabline 1
	GuiPopupmenu 1
endif
